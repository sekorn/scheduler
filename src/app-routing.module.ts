import {WelcomeComponent} from "./app/welcome/welcome.component";
import {RegisterComponent} from "./app/auth/register/register.component";
import {LoginComponent} from "./app/auth/login/login.component";
import {SchedulingComponent} from "./app/schedule/schedule.component";
import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {FieldListComponent} from "./app/locations/location/fields/field-list/field-list.component";
import {FieldsComponent} from "./app/locations/location/fields/fields.component";
import {AdminComponent} from "./app/admin/admin.component";
import {LocationsComponent} from "./app/locations/locations.component";
import {AuthGuard} from "./app/auth/auth.guard";
import {Register} from "ts-node";

const routes: Routes = [
  { path: '', component: WelcomeComponent, canActivate: [ AuthGuard ] },
  { path: 'signin', component: LoginComponent },
  { path: 'register' component: RegisterComponent, canActivate: [ AuthGuard ]},
  { path: 'schedule', component: SchedulingComponent, canActivate: [ AuthGuard ]},
  { path: 'locations', component: LocationsComponent, canActivate: [ AuthGuard ] },
  { path: 'fields', component: FieldsComponent canActivate: [ AuthGuard ]},
  { path: 'admin', component: AdminComponent, canActivate: [ AuthGuard ] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule {}

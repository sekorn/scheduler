import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {LoginComponent} from './auth/login/login.component';
import {RegisterComponent} from './auth/register/register.component';
import {WelcomeComponent} from './welcome/welcome.component';
import {AddLocationComponent} from './locations/location/add-location.component';
import {SchedulingComponent} from './schedule/schedule.component';
import {AppRoutingModule} from "../app-routing.module";
import {MaterialModule} from "../material.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FlexLayoutModule} from "@angular/flex-layout";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HeaderComponent} from './shared/navigation/header/header.component';
import {SidenavListComponent} from './shared/navigation/sidenav-list/sidenav-list.component';
import {FieldListComponent} from './locations/location/fields/field-list/field-list.component';
import {FieldsComponent} from './locations/location/fields/fields.component';
import {AdminComponent} from './admin/admin.component';
import {UsersComponent} from './admin/users/users.component';
import {EmailsComponent} from './admin/emails/emails.component';
import {LocationListComponent} from './locations/location-list/location-list.component';
import {LocationsComponent} from './locations/locations.component';
import {FieldComponent} from "./locations/location/fields/field/field.component";
import {LocationCardComponent} from './shared/location-card/location-card.component';
import {AuthService} from "./auth/auth.service";
import { LocationComponent } from './locations/location/location.component';
import { AddFieldComponent } from './locations/location/fields/field/add-field.component';
import {LocationService} from "./service/location.service";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    WelcomeComponent,
    FieldComponent,
    SchedulingComponent,
    HeaderComponent,
    SidenavListComponent,
    FieldListComponent,
    FieldsComponent,
    AdminComponent,
    UsersComponent,
    EmailsComponent,
    LocationListComponent,
    LocationsComponent,
    AddLocationComponent,
    LocationCardComponent,
    LocationComponent,
    AddFieldComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [AuthService, LocationService],
  bootstrap: [AppComponent]
})
export class AppModule {
}

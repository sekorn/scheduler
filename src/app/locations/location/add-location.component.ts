import { Component, OnInit } from '@angular/core';
import {FormGroup, FormArray, FormBuilder, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-location',
  templateUrl: 'add-location.component.html',
  styleUrls: ['add-location.component.css']
})
export class AddLocationComponent implements OnInit {
  public form: FormGroup;

  constructor(public _fb: FormBuilder) { }

  ngOnInit() {
    this.form = this._fb.group({
      name: ['', Validators.required ],
      abbreviation: [''],
      street: [''],
      city: [''],
      state: [''],
      zip: ['', Validators.minLength(5) ],
      mapLink: ['']
    });
  }

  save(model: Location) {
    console.log(model);
  }

  initField() {
    return this._fb.group({
      fieldname: ['', Validators.required ],
      slot: this._fb.array([
        this.initSlot()
      ])
    });
  }

  initSlot() {
    return this._fb.group({
      slotname: ['', Validators.required ],
      start: ['', Validators.required ],
      end: ['', Validators.required ],
      team: [''],
      opponent: [''],
      ref: ['']
    });
  }

  addField() {
    const control = <FormArray>this.form.controls.fields;
    control.push(this.initField())
  }

  removeField(i: number) {
    const control = <FormArray>this.form.controls.fields;
    control.removeAt(i)
  }

  addSlot(i: number) {
    const control = <FormArray>this.form.controls.fields[i];
    control.push(this.initSlot());
  }

  removeSlot(field: number, slot: number) {
    const control = <FormArray>this.form.controls.fields[field];
    control.removeAt(slot);
  }
}

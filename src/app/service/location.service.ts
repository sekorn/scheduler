import { Location } from './model/location';
import {Injectable} from "@angular/core";

@Injectable()
export class LocationService {
  availableLocations: Location[] = [
    { name: 'Delaware Valley Friends School', abbrev: 'DVFS'},
    { name: 'Cedar Hollow Park', abbrev: 'CH'},
    { name: 'Mill Road Park', abbrev: 'MR'},
    { name: 'Wilson Farm Park', abbrev: 'WFP'}
  ]
}

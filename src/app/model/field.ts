import {Slot} from "./slot";

export interface field {
  fieldname: string;
  slot: Slot[];
}

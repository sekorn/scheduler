import {field} from "./field";

export interface Location {
  name: string;
  abbreviation: string;
  street?: string;
  city?: string;
  state?: string;
  zip?: string;
  mapLink?: string;
  fields?: [field];
}

export interface Slot {
  slotname: string;
  start: string;
  end: string;
  team?: string;
  opponent?: string;
  ref?: string;
}
